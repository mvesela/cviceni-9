# Description
The `dev` branch is the main development branch. Merge all accepted feature branches to the `dev` branch or make pull/merge requests. If feature branch was merged, delete that branch. Keep updated your feature branches with current `dev` status.

# Requirements
## Development environment
* [BitBucket account](https://bitbucket.org/)
* [Git](https://git-scm.com/downloads)
* [SourceTree](https://www.sourcetreeapp.com/)
* [Node.js & npm](https://nodejs.org/en/) (use the LTS version)
* [Gulp](http://gulpjs.com/) (`npm install gulp-cli -g`)
* ... and text editor ie. [Visual Studio Code](https://code.visualstudio.com/)
    * Install [plugin which integrates ESLint into VS Code](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) using project config

## Start to develop
* Clone the repository from [HistoryLab team](https://bitbucket.org/historylab/cviceni)
* `npm start` in terminal to install & run the project in your browser

### Create new cvičení
`npm run new-cviceni -- --slug CVICENI-SLUG --name "CVICENI NAME"`

* `CVICENI-SLUG` is slugified ([kebab case](https://en.wikipedia.org/wiki/Kebab_case)) and **unique** identification string of cviceni
* `"CVICENI NAME"` is full title of cviceni

## Production build
* `npm run build` in terminal

## Code & design principles
* [The Zen of Python is a collection of 20 software principles](https://en.wikipedia.org/wiki/Zen_of_Python)
* [10 New Principles Of Good Design](https://www.fastcodesign.com/90154519/10-new-principles-of-good-design)
* [JavaScript Naming Conventions](https://www.robinwieruch.de/javascript-naming-conventions)
* [A successful Git branching model](https://nvie.com/posts/a-successful-git-branching-model/)

## License
[![Creative Commons License](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)  
This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).
